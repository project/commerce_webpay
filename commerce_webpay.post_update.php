<?php

/**
 * @file
 * Post update functions for Commerce Webpay.
 */

/**
 * Update the payment type of all previous payments with webpay.
 */
function commerce_webpay_post_update_payment_type_older_payments(&$sandbox) {
  /** @var \Drupal\commerce_payment\PaymentStorageInterface $storage */
  $storage = \Drupal::service('entity_type.manager')->getStorage('commerce_payment');
  if (!isset($sandbox['progress'])) {
    $sandbox['progress'] = 0;
    $sandbox['current_id'] = 0;
    $sandbox['max'] = $storage->getQuery()
      ->count()
      ->condition('type', 'payment_manual')
      ->condition('payment_gateway', 'webpay')
      ->execute();
  }

  $payments = $storage->getQuery()
    ->condition('type', 'payment_manual')
    ->condition('payment_gateway', 'webpay')
    ->condition('payment_id', $sandbox['current_id'], '>')
    ->range(0, 20)
    ->sort('payment_id')
    ->execute();

  foreach ($payments as $payment_id) {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $storage->load($payment_id);
    $payment->set('type', 'payment_webpay');
    $payment->save();
    // Clear cache of the payment to detect the new bundle.
    $storage->resetCache([$payment_id]);

    $payment = $storage->load($payment_id);
    $payment->set('webpay_transaction', $payment->getRemoteId());
    $payment->save();

    $sandbox['progress']++;
    $sandbox['current_id'] = $payment_id;
  }

  $sandbox['#finished'] = empty($sandbox['max']) ? 1 : ($sandbox['progress'] / $sandbox['max']);

  if ($sandbox['#finished'] < 1) {
    return t('Updating old payments with Webpay.');
  }

  return t('Updating old payments with Webpay finished');
}
