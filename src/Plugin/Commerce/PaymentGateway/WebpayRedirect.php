<?php

namespace Drupal\commerce_webpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webpay\Entity\WebpayConfig;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Entity\Payment;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "webpay",
 *   label = "Webpay",
 *   display_label = "Webpay",
 *   forms = {
 *     "offsite-payment" = "Drupal\commerce_webpay\PluginForm\OffsiteRedirect\PaymentOffsiteForm",
 *   },
 *   requires_billing_information = FALSE,
 *   payment_type = "payment_webpay",
 * )
 */
class WebpayRedirect extends OffsitePaymentGatewayBase {

  /**
   * The Webpay transaction storage class.
   *
   * @var \Drupal\webpay\WebpayTransactionStorageInterface
   */
  protected $webpayTransactionStorage;

  /**
   * The webpay config storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $webpayConfigStorage;

  /**
   * WebpayRedirect constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->webpayTransactionStorage = $this->entityTypeManager->getStorage('webpay_transaction');
    $this->webpayConfigStorage = $this->entityTypeManager->getStorage('webpay_config');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'webpay_config' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $configurations = [];

    $configs = $this->webpayConfigStorage->loadMultiple();
    foreach ($configs as $config) {
      $configurations[$config->id()] = $config->label();
    }
    $form['webpay_config'] = [
      '#type' => 'select',
      '#title' => $this->t('Webpay Config'),
      '#options' => $configurations,
      '#required' => TRUE,
      '#default_value' => $this->configuration['webpay_config'],
    ];

    // The mode is defined by the webpay config.
    $form['mode']['#access'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValue($form['#parents']);
    /** @var \Drupal\webpay\Entity\WebpayConfigInterface $webpay_config */
    $webpay_config = $this->webpayConfigStorage->load($values['webpay_config']);
    $this->configuration['webpay_config'] = $webpay_config->id();
    $this->configuration['mode'] = $webpay_config->getEnvironmentId();
  }

  /**
   * {@inheritdoc}
   */
  public function getSupportedModes() {
    return ['n/a' => $this->t('N/A')] + WebpayConfig::environments();
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $token = $request->request->get('token_ws');

    if ($token && ($transaction = $this->webpayTransactionStorage->byToken($token))) {
      if ($payment = Payment::load($transaction->get('session_id')->value)) {
        if ($payment->get('state')->value == 'completed') {
          return;
        }
      }
    }

    throw new PaymentGatewayException($this->t('We encountered an unexpected error processing your payment method. Please try again later.'));
  }

}
