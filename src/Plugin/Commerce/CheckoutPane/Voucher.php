<?php

namespace Drupal\commerce_webpay\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the voucher of the Webpay.
 *
 * @CommerceCheckoutPane(
 *   id = "webpay_voucher",
 *   label = @Translation("Webpay voucher"),
 *   default_step = "complete",
 * )
 */
class Voucher extends CheckoutPaneBase {

  /**
   * The payment storage class.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * The entity display repository.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Voucher constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface $checkout_flow
   *   The parent checkout flow.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $checkout_flow, $entity_type_manager);

    $this->paymentStorage = $this->entityTypeManager->getStorage('commerce_payment');
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $checkout_flow,
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'view_mode' => 'default',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationSummary() {
    $view_mode = $this->configuration['view_mode'];
    $view_modes = $this->entityDisplayRepository->getViewModeOptions('webpay_transaction');

    return $this->t('View mode: %view_mode', ['%view_mode' => isset($view_modes[$view_mode]) ? $view_modes[$view_mode] : $view_mode]);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#description' => $this->t('Defines the view mode that will be used to display the voucher.'),
      '#options' => $this->entityDisplayRepository->getViewModeOptions('webpay_transaction'),
      '#default_value' => $this->configuration['view_mode'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['view_mode'] = $values['view_mode'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    return $this->isPaymentGatewayWebpay();
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    if (!$this->isPaymentGatewayWebpay()) {
      return $pane_form;
    }

    $payments = $this->paymentStorage->loadMultipleByOrder($this->order);
    $view_builder = $this->entityTypeManager->getViewBuilder('webpay_transaction');
    foreach ($payments as $payment) {
      if ($payment->getPaymentGatewayId() == 'webpay' && $payment->getState()->value == 'completed' && !$payment->get('webpay_transaction')->isEmpty()) {
        if ($webpay_transaction = $payment->get('webpay_transaction')->entity) {
          $pane_form['webpay_voucher'][$webpay_transaction->id()] = $view_builder->view($webpay_transaction, $this->configuration['view_mode']);
        }
      }
    }

    return $pane_form;
  }

  /**
   * Checks if the payment gateway of the order is Webpay.
   *
   * @return bool
   *   Return TRUE if the payment gateway is Webpay, FALSE in otherwise.
   */
  protected function isPaymentGatewayWebpay() {
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->get('payment_gateway')->entity;

    return $payment_gateway && $payment_gateway->getPluginId() == 'webpay';
  }

}
