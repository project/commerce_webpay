<?php

namespace Drupal\commerce_webpay\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Provides the manual payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_webpay",
 *   label = @Translation("Webpay"),
 *   workflow = "payment_default",
 * )
 */
class PaymentWebpay extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    $fields['webpay_transaction'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel($this->t('Webpay transaction'))
      ->setDescription(t('The webpay transaction created from Webpay service.'))
      ->setSetting('target_type', 'webpay_transaction');

    return $fields;
  }

}
